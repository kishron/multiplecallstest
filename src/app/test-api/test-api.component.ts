import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, map, mergeMap, Observable, switchMap } from 'rxjs';

@Component({
  selector: 'app-test-api',
  templateUrl: './test-api.component.html',
  styleUrls: ['./test-api.component.scss']
})
export class TestApiComponent implements OnInit {

  urlBase = 'https://api-int.plananticorrupcion.co';
  tipos = '/api/tipos'
  entidadesXtipo = '/api/entidades/porTipo'
  plan = '/api/componentes'
  componentes = '/api/componentes'
  
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    ) {}


  ngOnInit(): void {
    this.route.params.pipe(
      switchMap( params => {
        return this.http.get(`${this.urlBase}${this.plan}/${params['entidadId']}`).pipe(
          map(plan => ({ nombre: 'plan 2023', id: 117})), // simulando un plan
          switchMap(plan => {
            return this.http.get(`${this.urlBase}${this.componentes}/${plan.id}`).pipe(
              map(response => { //este map simula los componentes
                console.log(response)
                return {
                  componentes: [
                    {id: 116}, 
                    {id: 117}, 
                    {id: 118}, 
                    {id: 119}, 
                    {id: 120}, 
                    {id: 121}
                  ]
                }
              }),
              switchMap(response => {
                console.log(response);
                let componentes$: Observable<unknown>[] = [];
                response.componentes.forEach(componente => {
                  componentes$.push(this.http.get(`${this.urlBase}${this.componentes}/${componente.id}`));
                })
                return forkJoin(componentes$)
              })
            )
          })

        )


          
        })
    ).subscribe(
      (responses) => console.log(responses)
    );
  }

}
