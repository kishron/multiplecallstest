import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { TestApiComponent } from './test-api/test-api.component';

const routes: Routes = [
    { path: '', redirectTo: '/home/116', pathMatch: 'full'},
    { path: 'home/:entidadId', component: TestApiComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
